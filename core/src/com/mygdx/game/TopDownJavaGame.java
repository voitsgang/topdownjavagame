package com.mygdx.game;


import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;

import com.mygdx.screens.GameMap;

import java.awt.*;

public class TopDownJavaGame extends Game {




	@Override
	public void create () {
	  setScreen(new GameMap());



	}

	@Override
	public void render () {
	  super.render();
	}

	@Override
	public void dispose () {
	  super.dispose();

	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}
}
