package com.mygdx.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.maps.tiled.*;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.mygdx.entities.Bullet;
import com.mygdx.entities.Player;
import com.mygdx.entities.SkeletonEnemy;

import java.rmi.server.Skeleton;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.concurrent.CopyOnWriteArrayList;

public class GameMap implements Screen
{

    private TiledMap map;
    private OrthogonalTiledMapRenderer renderer;
    private OrthographicCamera camera;
    //variable for the tileWidth and tileHeight (16*16)
    private int tileSize = 16;


    private Player player;

    //CopyOnWriteArrayList is used to avoid ConcurrentModificationException
    private CopyOnWriteArrayList<SkeletonEnemy> enemies = new CopyOnWriteArrayList<SkeletonEnemy>();
    private CopyOnWriteArrayList<SkeletonEnemy> removeEnemies = new CopyOnWriteArrayList<SkeletonEnemy>();
    private CopyOnWriteArrayList<Bullet> bulletList = new CopyOnWriteArrayList<Bullet>();

    private Animation skeletonWalking;

    //used for the size of the spritesheets 2*2
    private static final int FRAME_COLS = 2;
    private static final int FRAME_ROWS = 2;


    @Override
    public void show() {

        //loads the map and renders it as an OrthogonalTiledMap
        map = new TmxMapLoader().load("maps/finaltest.tmx");
        renderer = new OrthogonalTiledMapRenderer(map);

        //creates the camera for the map
        camera = new OrthographicCamera();


        //get the spritesheets for the player and the skeletons
        Texture playerWalksheet = new Texture(Gdx.files.internal("textures/mainCharacter/PlayerSpritesheet.png"));
        Texture skeletonWalksheet = new Texture(Gdx.files.internal("textures/enemies/SkeletonSpritesheet.png"));
        
        

        //Split the spritesheets into each frame and add them as TextureRegion to the 2D Array
        TextureRegion[][] playerTmp = TextureRegion.split(playerWalksheet,
                playerWalksheet.getWidth() / FRAME_COLS,
                playerWalksheet.getHeight() / FRAME_ROWS);

        TextureRegion[][] SkeletonTmp = TextureRegion.split(skeletonWalksheet,
                skeletonWalksheet.getWidth() / FRAME_COLS,
                skeletonWalksheet.getHeight() / FRAME_ROWS);


        

        //Create a 1-dimensional array for the animations so we can create an animation
        TextureRegion[] playerWalkFrames = new TextureRegion[FRAME_COLS * FRAME_ROWS];
        int playerIndex = 0;
        for (int i = 0; i < FRAME_ROWS; i++) {
            for (int j = 0; j < FRAME_COLS; j++) {
                playerWalkFrames[playerIndex++] = playerTmp[i][j];
            }
        }

        //create the player animation and set a frame duration for the animation
        Animation playerWalking = new Animation<TextureRegion>(1/6f, playerWalkFrames);

        TextureRegion[] skeletonWalkFrames = new TextureRegion[FRAME_COLS * FRAME_ROWS];
        int skeletonIndex = 0;
        for (int i = 0; i < FRAME_ROWS; i++) {
            for (int j = 0; j < FRAME_COLS; j++) {
                skeletonWalkFrames[skeletonIndex++] = SkeletonTmp[i][j];
            }
        }

        //create the skeleton animation an set a frame duration for the animation
        skeletonWalking = new Animation<TextureRegion>(1/6f,skeletonWalkFrames);

        //create the player, set its position and set the inputProcessor for the player, so that we can control him
        player = new Player(playerWalking,(TiledMapTileLayer) map.getLayers().get(0), map);
        player.setPosition(25 * player.getCollisionLayer().getTileWidth(), 40);
        Gdx.input.setInputProcessor(player);

        //calls the method to animate the torches
        animateTorch();
        //calls the method to spawn the enemies
        createEnemies();



    }

    @Override
    public void render(float delta) {
      Gdx.gl.glClearColor(0,0,0,1);
      Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

      //create a bullets when an arrow key is pressed and add them to the bullet list
      if(Gdx.input.isKeyJustPressed(Input.Keys.UP))
      {
          Vector2 position = new Vector2(player.getX(),player.getY());
          bulletList.add(new Bullet(new Sprite(new Texture("textures/details/shotUp.png")),"up",position));
      }
      if(Gdx.input.isKeyJustPressed(Input.Keys.DOWN))
      {
          Vector2 position = new Vector2(player.getX(),player.getY());
          bulletList.add(new Bullet(new Sprite(new Texture("textures/details/shotDown.png")),"down",position));
      }
      if(Gdx.input.isKeyJustPressed(Input.Keys.LEFT))
      {
          Vector2 position = new Vector2(player.getX(),player.getY());
          bulletList.add(new Bullet(new Sprite(new Texture("textures/details/shotLeft.png")),"left",position));
      }
      if(Gdx.input.isKeyJustPressed(Input.Keys.RIGHT))
      {
          Vector2 position = new Vector2(player.getX(),player.getY());
          bulletList.add(new Bullet(new Sprite(new Texture("textures/details/shotRight.png")),"right",position));
      }

      //set the camera position, that it always focuses the player and update it
      camera.position.set(player.getX() + player.getWidth() / 2, player.getY() + player.getHeight() / 2, 0);
      camera.update();

      //set the view for the renderer on the camera so that it can render everything on the screen
      renderer.setView(camera);
      renderer.render();



      //start the rendering process
      renderer.getBatch().begin();
      //render the player
      player.draw(renderer.getBatch());
      //render the bullets and if they are out of the screen, remove them, so they don't get rendered anymore
      for(Bullet bullet : bulletList)
      {

          if(bullet.getX() > Gdx.graphics.getWidth() || bullet.getY() > Gdx.graphics.getHeight())
          {
              bulletList.remove(bullet);
          }
          else
          {
              bullet.draw(renderer.getBatch());
          }
      }

      //check if a skeleton got hit by a bullet, if so, add them to the removeEnemies list
      for(SkeletonEnemy enemy : enemies)
      {
          for(Bullet bullet : bulletList)
          {
              if(((int) (bullet.getX() / tileSize) == (int) (enemy.getX() / tileSize)) && ((int) (bullet.getY() / tileSize) == (int) (enemy.getY() / tileSize)))
              {
                  removeEnemies.add(enemy);
              }
          }
      }

      //now remove all the enemies that got hit by bullets
      enemies.removeAll(removeEnemies);

      //render all the remaining enemies
      for(SkeletonEnemy enemy : enemies)
      {
          enemy.draw(renderer.getBatch());
      }
      //end the rendering process
      renderer.getBatch().end();




    }

    @Override
    public void resize(int width, int height) {
      camera.viewportWidth = width / 2;
      camera.viewportHeight = height / 2;
      camera.update();
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
      dispose();

    }

    @Override
    public void dispose() {
      map.dispose();
      renderer.dispose();
    }

    public void animateTorch()
    {
        //Animation

        //Frames for the animation
        Array<StaticTiledMapTile> frameTiles = new Array<StaticTiledMapTile>();

        //getting the frame tiles
        Iterator<TiledMapTile> tileList = map.getTileSets().getTileSet("torch").iterator();
        while(tileList.hasNext())
        {
            TiledMapTile tile = tileList.next();
            if(tile.getProperties().containsKey("animations") && tile.getProperties().get("animations").equals("torch"))
            {
                frameTiles.add((StaticTiledMapTile) tile);
            }
        }
        //create the animation
        AnimatedTiledMapTile animatedTile = new AnimatedTiledMapTile(1/3f,frameTiles);


        //Get the animation layer
        TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get("animationLayer");

        //replace the torches with animated torches
        for(int x = 0; x < layer.getWidth();x++)
        {
            for(int y =0; y < layer.getHeight();y++)
            {
                TiledMapTileLayer.Cell cell = layer.getCell(x,y);
                if(cell != null) {
                    if (cell.getTile().getProperties().containsKey("animations") && cell.getTile().getProperties().get("animations").equals("torch"))
                    {
                        cell.setTile(animatedTile);
                    }
                }

            }
        }
    }

    //method for creating all the enemies on the map
    public void createEnemies()
    {
        SkeletonEnemy enemy1 = new SkeletonEnemy(skeletonWalking);
        enemy1.setPosition(26 * player.getCollisionLayer().getTileWidth(), 250);
        SkeletonEnemy enemy2 = new SkeletonEnemy(skeletonWalking);
        enemy2.setPosition(26 * player.getCollisionLayer().getTileWidth(), 600);
        SkeletonEnemy enemy3 = new SkeletonEnemy(skeletonWalking);
        enemy3.setPosition(12 * player.getCollisionLayer().getTileWidth(), 600);
        SkeletonEnemy enemy4 = new SkeletonEnemy(skeletonWalking);
        enemy4.setPosition(35 * player.getCollisionLayer().getTileWidth(), 600);
        enemies.add(enemy1);
        enemies.add(enemy2);
        enemies.add(enemy3);
        enemies.add(enemy4);
    }


}
