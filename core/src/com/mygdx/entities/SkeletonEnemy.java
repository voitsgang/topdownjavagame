package com.mygdx.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

public class SkeletonEnemy extends Sprite
{
    private float animationTime = 0;
    private Animation walk;
    boolean turn = false;
    int count;

    public SkeletonEnemy(Animation walk)
    {
        super((TextureRegion) walk.getKeyFrame(0));
        walk.setPlayMode(Animation.PlayMode.LOOP);
        this.walk = walk;
        setScale(2);
    }

    @Override
    public void draw(Batch batch) {
        update(Gdx.graphics.getDeltaTime());
        setRegion((TextureRegion) walk.getKeyFrame(animationTime));
        animationTime = animationTime == 4 ? animationTime = 0 : animationTime++;
        super.draw(batch);


    }

    public void update(float delta)
    {
        //if the count equals 200: set the count to 0 and change the direction of the skeleton by changing the boolean
        if(count == 200)
        {
            count = 0;
            turn = !turn;
        }

        if(turn)
        {
            setX((float) (getX() + 0.5)); // move 0.5 pixels to the right
            count++; // increment the counter
        }
        else
        {
            setX((float) (getX() - 0.5)); // move 0.5 pixels to the left
            count++; // increment the counter
        }

        animationTime += delta; // add the deltaTime that is set by setFrameDuration() to the animationTim
    }
}
