package com.mygdx.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.*;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTile;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.tiles.AnimatedTiledMapTile;
import com.badlogic.gdx.maps.tiled.tiles.StaticTiledMapTile;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

import java.util.Iterator;

public class Player extends Sprite implements InputProcessor
{
    private Vector2 velocity = new Vector2(); // The velocity of the player
    private float animationTime = 0; // animation times for the frames
    private float speed  = 60 * 2; // Maximum speed of the player

    private Animation walk;
    private TiledMap map;


    private float increment;

    private float tileWidth;
    private float tileHeight;

    private String blockedKey = "blocked";




    private TiledMapTileLayer collisionLayer;

    public Player (Animation walk, TiledMapTileLayer collisionLayer, TiledMap map)
    {
        super((TextureRegion) walk.getKeyFrame(0));
        //sets the animation into loop mode
        walk.setPlayMode(Animation.PlayMode.LOOP);
        this.walk = walk;

        this.collisionLayer = collisionLayer;
        this.map = map;

        tileWidth = collisionLayer.getTileWidth();
        tileHeight = collisionLayer.getTileHeight();

        setScale(2);


    }

    @Override
    public void draw(Batch batch) {
        update(Gdx.graphics.getDeltaTime());
        //set the frame according to the animationTime
        setRegion((TextureRegion) walk.getKeyFrame(animationTime));
        //increments the animationTime until the last frame and then starts with the first frame again
        animationTime = animationTime == 4 ? animationTime = 0 : animationTime++;
        super.draw(batch);
    }

    public void update (float delta) //calculates new position of the player
    {
        //save old position
        float oldX = getX();
        float oldY = getY();


        boolean collisionX = false;
        boolean collisionY = false;


        //variable to check if the player collides on the x-axis or the y-axis
        boolean collisionOnX = false;
        boolean collisionOnY = false;


        //set position on the x-axis
        setX(getX() + velocity.x * delta);

        // calculate the increment for step in #collidesLeft() and #collidesRight()
        increment = collisionLayer.getTileWidth();
        increment = getWidth() < increment ? getWidth() / 2 : increment / 2;

        if(velocity.x < 0) // going left
            collisionX = collidesLeft();
        else if(velocity.x > 0) // going right
            collisionX = collidesRight();

        // react to x collision
        if(collisionX) {
            setX(oldX);
            velocity.x = 0;
        }



        //set position on the y-axis
        setY(getY() + velocity.y * delta);

        // calculate the increment for step in collidesBottom() and collidesTop()
        increment = collisionLayer.getTileHeight();
        increment = getHeight() < increment ? getHeight() / 2 : increment / 2;

        if(velocity.y < 0) // going down
            collisionY = collidesBottom();
        else if(velocity.y > 0) // going up
            collisionY = collidesTop();

        // react to y collision
        if(collisionY) {
            setY(oldY);
            velocity.y = 0;
        }

        animationTime +=delta;

    }



    public void openDoor()
    {
        //Frames for the animation
        Array<StaticTiledMapTile> frameTiles = new Array<StaticTiledMapTile>();
        StaticTiledMapTile leftOpenDoor = null;
        StaticTiledMapTile rightOpenDoor = null;
        StaticTiledMapTile centerOpenDoor = null;


        //getting the frame tiles by checking if it contains the property "replace"
        Iterator<TiledMapTile> tileList = map.getTileSets().getTileSet("tiles").iterator();
        while(tileList.hasNext())
        {
            TiledMapTile tile = tileList.next();

            if(tile.getProperties().containsKey("replace"))
            {
                if(tile.getProperties().get("replace").equals("left"))
                {
                    leftOpenDoor = (StaticTiledMapTile) tile;
                }
                else if(tile.getProperties().get("replace").equals("right"))
                {
                    rightOpenDoor = (StaticTiledMapTile) tile;
                }
                else if(tile.getProperties().get("replace").equals("center"))
                {
                    centerOpenDoor = (StaticTiledMapTile) tile;
                }
            }

        }

        //Get the layer
        TiledMapTileLayer layer = (TiledMapTileLayer) map.getLayers().get("background");
        // check the area around the player
        for(int x = (int) ((getX() + getWidth() / 2) / tileWidth) - 2; x <= (int) ((getX() + getWidth() / 2) / tileWidth) + 2;x++)
        {
            for(int y = (int) ((getY() + getHeight() / 2) /tileHeight) - 2 ; y < (int) ((getY() + getHeight() / 2) / tileHeight) + 2;y++)
            {
                TiledMapTileLayer.Cell cell = layer.getCell(x,y);
                if(cell != null)
                {
                    if (cell.getTile().getProperties().containsKey("door") && cell.getTile().getProperties().get("door").equals("horizontal"))
                    {
                        if(cell.getTile().getProperties().containsKey("closed"))
                        {
                            if(cell.getTile().getProperties().get("closed").equals("left"))
                            {
                                cell.setTile(leftOpenDoor);
                            }
                            else if(cell.getTile().getProperties().get("closed").equals("center"))
                            {
                                cell.setTile(centerOpenDoor);
                            }
                            else if(cell.getTile().getProperties().get("closed").equals("right"))
                            {
                                cell.setTile(rightOpenDoor);
                            }
                        }
                    }
                }

            }
        }
    }

    //checks if the tile with the given coordinates hast the "blocked" attribute
    private boolean isCellBlocked(float x, float y) {
        TiledMapTileLayer.Cell cell = collisionLayer.getCell((int) (x / collisionLayer.getTileWidth()), (int) (y / collisionLayer.getTileHeight()));
        return cell != null && cell.getTile() != null && cell.getTile().getProperties().containsKey(blockedKey);
    }


    //checks for collision on the right side of the player
    public boolean collidesRight() {
        for(float step = 0; step <= getHeight(); step += increment)
            if(isCellBlocked(getX() + getWidth(), getY() + step))
                return true;
        return false;
    }
    //checks for collision on the left side of the player
    public boolean collidesLeft() {
        for(float step = 0; step <= getHeight(); step += increment)
            if(isCellBlocked(getX(), getY() + step))
                return true;
        return false;
    }

    //checks for collision on the right side of the player
    public boolean collidesTop() {
        for(float step = 0; step <= getWidth(); step += increment)
            if(isCellBlocked(getX() + step, getY() + getHeight()))
                return true;
        return false;

    }

    //checks for collision under the player
    public boolean collidesBottom() {
        for(float step = 0; step <= getWidth(); step += increment)
            if(isCellBlocked(getX() + step, getY()))
                return true;
        return false;
    }


    @Override
    public boolean keyDown(int keycode)
    {
        switch(keycode)
        {
            case Input.Keys.W: velocity.y = speed;  break;
            case Input.Keys.A: velocity.x = -speed; break;
            case Input.Keys.S: velocity.y = -speed; break;
            case Input.Keys.D: velocity.x = speed;  break;
        }


        return true;
    }

    @Override
    public boolean keyUp(int keycode) {

        switch(keycode)
        {
            case Input.Keys.W:
            case Input.Keys.S:
                velocity.y = 0;break;
            case Input.Keys.A:
            case Input.Keys.D:
                velocity.x = 0;break;
            case Input.Keys.SPACE: openDoor();break;
        }

        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    public Vector2 getVelocity() {
        return velocity;
    }

    public void setVelocity(Vector2 velocity) {
        this.velocity = velocity;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public TiledMapTileLayer getCollisionLayer() {
        return collisionLayer;
    }

    public void setCollisionLayer(TiledMapTileLayer collisionLayer) {
        this.collisionLayer = collisionLayer;
    }
}


