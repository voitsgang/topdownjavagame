package com.mygdx.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

public class Bullet extends Sprite
{
    public static final int SPEED = 150;
    private static Texture texture;

    private Vector2 shotLine = new Vector2();
    private String direction;
    private Vector2 position;


    public boolean remove = false;

    public Bullet(Sprite sprite, String direction, Vector2 position)
    {
        super(sprite);
        this.direction = direction;
        this.position = position;

        setSize(32,32);

    }

    @Override
    public void draw(Batch batch)
    {
        update(Gdx.graphics.getDeltaTime());
        super.draw(batch);
    }

    public void update (float deltaTime)
    {

        //shoot the bullet to the top
        if(direction.equals("up"))
        {
            position.y += 10;
        }
        //shoot the bullet down
        if(direction.equals("down"))
        {
            position.y += -10;
        }
        //shoot the bullet left
        if(direction.equals("left"))
        {
            position.x  += -10;
        }
        //shoot the bullet right
        if(direction.equals("right"))
        {
            position.x += 10;
        }


        //set the position of the bullet
        setX(position.x);
        setY(position.y);


    }

}
